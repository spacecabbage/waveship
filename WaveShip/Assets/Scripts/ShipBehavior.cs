﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehavior : MonoBehaviour {

    public bool isVulnerable;

    [SerializeField]
    private int maxHealth;

    [SerializeField]
    private int currentHealth;
    public int HP { get { return currentHealth; } }

    [SerializeField]
    private Collider col;

    void DecreaseHealth(int amountLost)
    {
        if (isVulnerable)
        {
            currentHealth -= amountLost;
            GameManager.instance.shipAnimation.animationShip.Play("hit");
            if (currentHealth <= 0)
            {
                Die();
            } else
            {
                if (GameManager.instance.shipBehavior.HP > 0) GameManager.instance.shipAnimation.animationShip.PlayQueued("idle");
            }
        }
    }

    void Die ()
    {
        isVulnerable = false;
        GameManager.instance.playerInput.DisableInput();
        GameManager.instance.waveGenerator.StopMovingWaves();
        GameManager.instance.looseStateLogic.GameOver();
    } 

	// Use this for initialization
	void Awake () {
        currentHealth = maxHealth;
	}

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("collided with " + other.gameObject.transform.parent);
        if (other.gameObject.transform.parent.GetComponent<WaveObject>().waveType != waveTypes.Empty)
        {
            Debug.Log("we've been hit!");
            other.gameObject.transform.parent.GetComponent<WaveObject>().Neutralize();
            DecreaseHealth(1);
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
