﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveObject : MonoBehaviour {

    public Renderer waveRenderer;
    public float waveRendererTimer;
    public Animation waveAnim;

    public waveTypes waveType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateShining();

    }

    public void EnableShining()
    {
        waveRendererTimer = 0.01f;
    }
            
    public void UpdateShining()
    {
        if (waveRendererTimer > 0)
        {
            waveRendererTimer = Mathf.Clamp01(waveRendererTimer);
            waveRendererTimer += Time.deltaTime;
            waveRenderer.material.SetFloat("_Blend", GameManager.instance.waveChecker.curveOfShining.Evaluate(waveRendererTimer));

        }
        if (waveRendererTimer == 1)
        {
            waveRendererTimer = 0f;
        }

    }

    public void Neutralize()
    {
        waveAnim.Play("calmVawe");
         waveAnim.PlayQueued("calmIdle");
        GameManager.instance.screenShake.ScreenShakeShake();
        waveType = waveTypes.Empty;
    }

    
}
