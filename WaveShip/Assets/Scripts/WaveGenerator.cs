﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour {

    public bool gameRunning = true;

    public bool randomWaves;
    public int currentStage;
    public int currentWave;

    public int screenWidthInWaves;

    public float waveDistance;
    public float despawnPosition;
    public List<GameObject> WaveList;
    public GameObject spawnObject;
    public GameObject lastWaveSpawned;

    [SerializeField]
    private List<ObjectPooler> poolerReferences;

    void Start() {
        for (int j = 0; j <= screenWidthInWaves; j++)
        {
            CreateWave(waveTypes.Empty);
        }
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            CreateWave(waveTypes.Empty);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            CreateWave(waveTypes.Rect);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            CreateWave(waveTypes.Sinus);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            CreateWave(waveTypes.Triangle);
        }

        if (gameRunning)
        {
            UpdateAllWaves();
        }
    }

    public void UpdateAllWaves()
    {
        for (int i = 0; i < WaveList.Count; i++)
        {
            UpdateWavePosition(WaveList[i]);
        }
    }

    public void UpdateWavePosition(GameObject waveObject)
    {
        waveObject.transform.position += new Vector3(-GameManager.instance.stageManager.stages[GameManager.instance.stageManager.CurrentStage].speed * Time.deltaTime, 0f, 0f);
        //checkForDespawn
        if (waveObject.transform.position.x < despawnPosition)
        {
            waveObject.SetActive(false);
            WaveList.Remove(waveObject);
            if (WaveList.Count < screenWidthInWaves)
            {
                AddNewWave();
            }
        }
    }

    public void CreateWave(waveTypes waveType)
    {
        GameObject newWave = null;
        switch (waveType)
        {
            case waveTypes.Empty:
                newWave = poolerReferences[(int)waveTypes.Empty].GetAnObjectFromThePool();
                break;
            case waveTypes.Sinus:
                newWave = poolerReferences[(int)waveTypes.Sinus].GetAnObjectFromThePool();
                break;
            case waveTypes.Rect:
                newWave = poolerReferences[(int)waveTypes.Rect].GetAnObjectFromThePool();
                break;
            case waveTypes.Triangle:
                newWave = poolerReferences[(int)waveTypes.Triangle].GetAnObjectFromThePool();
                break;
        }
        if (waveType != waveTypes.Empty)
        {
            newWave.GetComponent<WaveObject>().waveType = waveType;
        }
        newWave.SetActive(true);

        WaveList.Add(newWave);


        // if something in the list attach yourself to something

        if (WaveList.Count > 0 && lastWaveSpawned != null)
        {
            newWave.transform.position = lastWaveSpawned.transform.position;
            newWave.transform.position += new Vector3(waveDistance, 0f, 0f);
        }
        else
        {
            //newWave.transform.position = this.transform.position;

            newWave.transform.position = spawnObject.transform.position;

        }

        newWave.transform.localScale = spawnObject.transform.localScale;

        lastWaveSpawned = newWave;

        //newWave.GetComponent<WaveObject>().Initialize();
    }

    void AddNewWave()
    {
        if (randomWaves) {
            float random = Random.value;
            if (random < 0.11f) {
                CreateWave(waveTypes.Sinus);
            } else if (random < 0.22f) {
                CreateWave(waveTypes.Triangle);
            } else if (random < 0.33f) {
                CreateWave(waveTypes.Rect);
            } else {
                CreateWave(waveTypes.Empty);
            }
            return;
        }

        CreateWave(GameManager.instance.stageManager.stages[currentStage].waves[currentWave]);
        if (currentWave < GameManager.instance.stageManager.stages[currentStage].waves.Count - 1)
        {
            currentWave += 1;
        }
        else
        {
            if (currentStage < GameManager.instance.stageManager.stages.Length - 1)
            {
                currentStage += 1;
                currentWave = 0;
            }
            else
            {
                StopMovingWaves();
                GameManager.instance.looseStateLogic.GameWon();
            }
        }
    }

    public void StopMovingWaves()
    {
        gameRunning = false;
    }
}
