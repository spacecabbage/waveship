﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {

    public NotificationManager notificationManager;
    public  float endLevelTimer;
    public enum NextSceneType {startGame,freedom,exit };
    public NextSceneType nextScene;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateEndLevelTimer();
        AnyButtonPressed();
    }

    public void AnyButtonPressed()
    {
        if (Input.GetButtonDown("A") || Input.GetKeyDown(KeyCode.Q)
            ||
            Input.GetButtonDown("B") || Input.GetKeyDown(KeyCode.W)
            ||
            Input.GetButtonDown("Y") || Input.GetKeyDown(KeyCode.E)
            ||
            Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.R)
            )
        {
            FadeToNextLevel(NextSceneType.startGame);
        }
    }

    public void UpdateEndLevelTimer()
    {
        if (endLevelTimer > 0f)
        {
            endLevelTimer += Time.deltaTime;
        }

        if (endLevelTimer > 0.9f)
        {
           
            GoToNextLevel(nextScene);
        }

        
    }

    public void FadeToNextLevel(NextSceneType nextSceneType)
    {
        nextScene = nextSceneType;
        endLevelTimer = 0.01f;
        notificationManager.notifications[1].onOff.On = false;
        notificationManager.notifications[2].onOff.On = false;
    }


    public void GoToNextLevel(NextSceneType nextSceneType)
    {
        switch (nextSceneType)
        {
            case NextSceneType.startGame:
                SceneManager.LoadScene("GameScene");
                break;
            case NextSceneType.freedom:

                break;
            case NextSceneType.exit:

                break;


        }
    }
}
