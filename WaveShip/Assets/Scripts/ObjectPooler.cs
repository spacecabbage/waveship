﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    public GameObject masterObject;
    public GameObject objectToSpawn;
    public int amountToSpawn;
    public bool spawnMoreIfNecessary = true;

    public List<GameObject> currentOjbectPool;

    void Start()
    {
        currentOjbectPool = new List<GameObject>();

        for (int i = 0; i < amountToSpawn; i++)
        {
            CreateNewObject();
        }
    }

    public GameObject GetAnObjectFromThePool()
    {
        for (int i = 0; i < currentOjbectPool.Count; i++)
        {
            if (!currentOjbectPool[i].activeInHierarchy)
            {
                return currentOjbectPool[i];
            }
        }

        if (spawnMoreIfNecessary)
        {
            amountToSpawn++;
            return CreateNewObject();
        }

        return null;
	}

    GameObject CreateNewObject ()
    {
        GameObject obj = (GameObject)Instantiate(objectToSpawn);
        obj.transform.parent = masterObject.transform;
        obj.SetActive(false);
        currentOjbectPool.Add(obj);

        return obj;
    }
}
