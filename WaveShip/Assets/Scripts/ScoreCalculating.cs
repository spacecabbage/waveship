﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCalculating : MonoBehaviour {

    public int currentScore = 0;
    public static int highScore;
    public int minScore;
    public int maxScore;
    public float minDist;
    public float maxDist;
    public EasyFontTextMesh easyFontScore;
    public EasyFontTextMesh easyFontHighScore;
    [SerializeField]
    private Text scoreText;

    public void EvaluateScore(float distance)
    {
        distance = Mathf.Abs(distance);

        if (distance <= minDist)
        {
            currentScore += maxScore;
            GameManager.instance.waveChecker.FireConfetti();
        }
        else if (distance >= maxDist)
        {
            currentScore += minScore;
        }
        else
        {
            currentScore += Mathf.FloorToInt(Mathf.Lerp(maxScore, minScore, (distance - minDist) / (maxDist - minDist)));
        }
        UpdateScoreIndicator();
        if (currentScore > highScore)
        {
            highScore = currentScore;
            UpdateHighScoreIndicator();
        }
    }

    void UpdateScoreIndicator()
    {
        scoreText.text = "Score: " + currentScore;
        easyFontScore.Text = "Score: " + currentScore;
    }

    void UpdateHighScoreIndicator()
    {
        easyFontHighScore.Text = "HighScore: " + highScore;
    }

    void Awake()
    {
        if (highScore != 0)
        {
            UpdateHighScoreIndicator();
        }
    }
}
