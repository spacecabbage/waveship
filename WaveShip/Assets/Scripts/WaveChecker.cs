﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveChecker : MonoBehaviour {

    public waveTypes currentCheckedWaveType;
    public WaveObject currentCheckedWave;
    public AnimationCurve curveOfShining;
    public Color[] colors;
    public Color endColor;
    public Color endColorStorm;
    public float[] colorForces;
    public float[] colorStormForces;
    public int selectedColor;
    public int selectedStormColor;
    public float forceBuildup = 1f;
    public Material godrayMat;
    public Material raveColorMat;
    public float raveStormResetTimer;
    public ParticleSystem confetti1;
    public ParticleSystem confetti2;


    [SerializeField]
    private Transform waveDetector;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
        bool inputRect = Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.E);
        bool inputSinus = Input.GetButtonDown("B") || Input.GetKeyDown(KeyCode.Q);
        bool inputTriangle = Input.GetButtonDown("Y") || Input.GetKeyDown(KeyCode.W);
        if (inputRect && currentCheckedWaveType == waveTypes.Rect)
        {
            DestroyCurrentWave();
            Debug.Log("Hit Rect");
        }
        if (inputSinus && currentCheckedWaveType == waveTypes.Sinus)
        {
            DestroyCurrentWave();
            Debug.Log("Hit Sinus");
        }
        if (inputTriangle && currentCheckedWaveType == waveTypes.Triangle)
        {
            DestroyCurrentWave();
            Debug.Log("Hit Triangle");
        }
        UpdateColorForces();
        UpdateColorForcesStorm();
    }

    public void UpdateColorForces()
    {
        for (int i = 0; i < colors.Length; i++)
        {
            if (i == selectedColor)
            {
                colorForces[i] += Time.deltaTime * forceBuildup;
            }
            else
            {
                colorForces[i] -= Time.deltaTime * forceBuildup;
            }
            colorForces[i] = Mathf.Clamp01(colorForces[i]);
        }

        endColor = new Color(0f, 0f, 0f, 0f); ;
        for (int i = 0; i < colors.Length; i++)
        {
            float force = colorForces[i];
            endColor += new Color(colors[i].r * force, colors[i].g * force, colors[i].b * force, colors[i].a * force);
        }
        godrayMat.SetColor("_TintColor", endColor);

    }

    public void UpdateColorForcesStorm()
    {
        for (int i = 0; i < colors.Length; i++)
        {
            if (i == selectedStormColor)
            {
                colorStormForces[i] += Time.deltaTime * forceBuildup;
            }
            else
            {
                colorStormForces[i] -= Time.deltaTime * forceBuildup;
            }
            colorStormForces[i] = Mathf.Clamp01(colorStormForces[i]);
        }

        endColorStorm = new Color(0f, 0f, 0f, 0f); ;
        for (int i = 0; i < colors.Length; i++)
        {
            float force = colorStormForces[i];
            endColorStorm += new Color(colors[i].r * force, colors[i].g * force, colors[i].b * force, colors[i].a * force*0.5f);
        }
        raveColorMat.SetColor("_TintColor", endColorStorm);

        if (raveStormResetTimer > 0)
        {
            raveStormResetTimer += Time.deltaTime;
            if (raveStormResetTimer > 0.3f)
            {
                raveStormResetTimer = 0f;
                selectedStormColor = 4;
            }
        }
    }

    


    void DestroyCurrentWave()
    {
        GameManager.instance.waveChecker.selectedStormColor = (int)currentCheckedWave.waveType;
        GameManager.instance.scoreCalculating.EvaluateScore(waveDetector.position.x - currentCheckedWave.transform.position.x);
        Debug.Log("Interacted with wave at a distance of: " + (waveDetector.position.x - currentCheckedWave.transform.position.x));
        currentCheckedWave.Neutralize();
        
        GameManager.instance.waveChecker.raveStormResetTimer = 0.01f;

        currentCheckedWaveType = waveTypes.Empty;
    } 

    public void FireConfetti()
    {
        confetti1.Play();
        confetti2.Play();
    }
}
