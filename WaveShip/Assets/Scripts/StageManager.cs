﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum waveTypes { Empty, Sinus, Rect, Triangle };

public class StageManager : MonoBehaviour {

    public int CurrentStage;
    public Stage[] stages;

    // Use this for initialization
    void Start () {
        BookendStagesWithEmptyWaves();
    }

    void BookendStagesWithEmptyWaves()
    {
        ProcessWaveLists();
        foreach (Stage stage in stages)
        {
            for (int b = 0; b < stage.emptyWavesBefore; b++)
            {
                stage.waves.Insert(0, waveTypes.Empty);
            }
            for (int e = 0; e < stage.emptyWavesAfter; e++)
            {
                stage.waves.Insert(stage.waves.Count, waveTypes.Empty);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    [System.Serializable]
    public class Stage
    {
        public string name;
        public float bpm;
        public float speed;
        public int emptyWavesBefore;
        [TextArea(0,6)]
        public string waveList;
        public int emptyWavesAfter;
        public List<waveTypes> waves;
        // public WaveCollection[] waveCollection;

        //[System.Serializable]
        //public class WaveCollection
        //{
        //    public waveTypes[] levelSnippet;


        //}
    }

    void ProcessWaveLists()
    {
        foreach (Stage st in stages)
        {
            //generate waves from char list
            foreach (char c in st.waveList)
            {
                if (c == '1')
                {
                    st.waves.Add(waveTypes.Sinus);
                } else if (c == '2')
                {
                    st.waves.Add(waveTypes.Triangle);
                } else if (c == '3')
                {
                    st.waves.Add(waveTypes.Rect);
                } else
                {
                    st.waves.Add(waveTypes.Empty);
                }
            }
            //set stage speed from bpm
            st.speed = GameManager.instance.waveGenerator.waveDistance * (st.bpm / 60f);
        }
    }
}
