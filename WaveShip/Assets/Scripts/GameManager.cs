﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    public StageManager stageManager;
    public PlayerInput playerInput;
    public ShipAnimation shipAnimation;
    public WaveChecker waveChecker;
    public WaveGenerator waveGenerator;
    public NotificationManager notificationManager;
    public looseState looseStateLogic;
    public ScoreCalculating scoreCalculating;
    public AudioSource effects;
    public ShipBehavior shipBehavior;
    public ScreenShake screenShake;

    public void Awake()
    {
        instance = this;
    }
    
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
