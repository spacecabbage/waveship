﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour {

   //public
    public float shakeTimer;
    public AnimationCurve shakeCurve;
    public float shakeRange = 2f;
    //private
    private Vector3 startPostition;

    // Use this for initialization
    void Start () {
        startPostition = this.gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        UpdateShake();


    }

    public void ScreenShakeShake()
    {
        shakeTimer = 0.01f;
    }

    public void UpdateShake()
    {
        if (shakeTimer > 0)
        {
            shakeTimer += Time.deltaTime;
        }

        Vector3 shakeAmount = (new Vector3(0f, shakeRange, 0f) * shakeCurve.Evaluate(shakeTimer*2));
        this.transform.position = startPostition + shakeAmount;
        //this.transform.position -= ((new Vector3(0f, shakeRange, 0f) * shakeCurve.Evaluate(shakeTimer)) * 0.5f);
    }
}
