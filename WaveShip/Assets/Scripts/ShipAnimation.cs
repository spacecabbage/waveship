﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAnimation : MonoBehaviour {

    public ParticleSystem[] particles;
    public Animation animationShip;
    public GameObject ourNormalShip;
    public GameObject ourGameOverShip;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EmitRadioParticle(waveTypes type)
    {

        int selectedParticle = ((int)type) * 2;
        particles[selectedParticle-1].Play();
        particles[selectedParticle-2].Play();
        //particles[((int)type - 1)*2].Play();
        animationShip.Play("jump");
        animationShip.PlayQueued("idle");
    }
}
