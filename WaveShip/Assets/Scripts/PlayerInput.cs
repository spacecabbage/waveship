﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    [SerializeField]
    private bool acceptingInput = true;
    [SerializeField]
    private float inputCooldownDuration;
    [SerializeField]
    private float inputCooldownTimeLeft;

    //public AudioClip[] effectsSource;
    public SoundCollection[] effectCollection;

    [System.Serializable]
    public class SoundCollection
    {
        public string name;
        public AudioClip[] effectsSource;

        public AudioClip GetRandomClip()
        {
            int random = Random.Range(0,effectsSource.Length);
            return effectsSource[random];
        }
    }

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        if (acceptingInput)
        {
            if (inputCooldownTimeLeft <= 0f)
            {
                if (Input.GetButtonDown("A") || Input.GetKeyDown(KeyCode.R))
                {
                    Debug.Log("Pressed A");
                    /*inputCooldownTimeLeft = inputCooldownDuration;
                    GameManager.instance.shipAnimation.EmitRadioParticle(waveTypes.Rect);
                    //GameManager.instance.effects.clip = effectsSource[0];
                    GameManager.instance.effects.clip = effectCollection[0].GetRandomClip();
                    GameManager.instance.effects.Play();*/
                }

                if (Input.GetButtonDown("B") || Input.GetKeyDown(KeyCode.Q))
                {
                    Debug.Log("Pressed B");
                    inputCooldownTimeLeft = inputCooldownDuration;
                    GameManager.instance.shipAnimation.EmitRadioParticle(waveTypes.Sinus);
                    // GameManager.instance.effects.clip = effectsSource[1];
                    GameManager.instance.effects.clip = effectCollection[1].GetRandomClip();
                    GameManager.instance.effects.Play();
                }

                if (Input.GetButtonDown("Y") || Input.GetKeyDown(KeyCode.W))
                {
                    Debug.Log("Pressed Y");
                    inputCooldownTimeLeft = inputCooldownDuration;
                    GameManager.instance.shipAnimation.EmitRadioParticle(waveTypes.Triangle);
                    GameManager.instance.effects.clip = effectCollection[2].GetRandomClip();
                    //GameManager.instance.effects.clip = effectsSource[2];

                    GameManager.instance.effects.Play();
                }

                if (Input.GetButtonDown("X") || Input.GetKeyDown(KeyCode.E))
                {
                    Debug.Log("Pressed X");
                    inputCooldownTimeLeft = inputCooldownDuration;
                    GameManager.instance.shipAnimation.EmitRadioParticle(waveTypes.Rect);
                    //GameManager.instance.effects.clip = effectsSource[0];
                    GameManager.instance.effects.clip = effectCollection[0].GetRandomClip();
                    GameManager.instance.effects.Play();
                }
            }
            else
            {
                inputCooldownTimeLeft -= Time.deltaTime;
            }
        }
    }

    public void DisableInput()
    {
        acceptingInput = false;
    }
}
