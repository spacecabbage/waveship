﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class looseState : MonoBehaviour {

    public GameObject looseSign;

    public float reloadInputDelay;
    public bool acceptReloadInput;

    public void GameOver()
    {
       // GameManager.instance.notificationManager.notifications[0].onOff.On = true;

        GameManager.instance.notificationManager.notifications[3].onOff.On = true;
        GameManager.instance.notificationManager.notifications[4].onOff.On = true;
        //GameManager.instance.waveGenerator.StopMovingWaves();
        StartCoroutine(DelayReloadInput());
        GameManager.instance.shipAnimation.animationShip.Stop("idle");
        GameManager.instance.shipAnimation.animationShip.Stop("hit");
        GameManager.instance.shipAnimation.animationShip.Stop();
        GameManager.instance.shipAnimation.ourNormalShip.SetActive(false);
        GameManager.instance.shipAnimation.ourGameOverShip.SetActive(true);
        GameManager.instance.shipAnimation.ourGameOverShip.GetComponent<Animation>().Play("sink");
       // GameManager.instance.shipAnimation.animationShip.Play("sink");
    }

    public void GameWon ()
    {
        StartCoroutine(DelayReloadInput());
        GameManager.instance.notificationManager.notifications[5].onOff.On = true;
    }

    private void Start()
    {
        looseSign.SetActive(true);
    }

    // Update is called once per frame
	void Update () {
        if (acceptReloadInput)
        {
            bool input = Input.GetMouseButtonDown(0) || Input.GetButtonDown("A") || Input.GetKeyDown(KeyCode.Q) || Input.GetButtonDown("B") || Input.GetKeyDown(KeyCode.W) || Input.GetButtonDown("Y") || Input.GetKeyDown(KeyCode.E);
            if (input)
            {
                //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                SceneManager.LoadScene("StartScene");
            }
        }
	}

    IEnumerator DelayReloadInput()
    {
        while (reloadInputDelay > 0f)
        {
            reloadInputDelay -= Time.deltaTime;
            yield return null;
        }
        acceptReloadInput = true;
    }
}
