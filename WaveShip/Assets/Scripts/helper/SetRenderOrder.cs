﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRenderOrder : MonoBehaviour {

    public int renderOrder;

	// Use this for initialization
	void Start () {

        GetComponent<Renderer>().material.renderQueue = renderOrder;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
