﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class NotificationManager : MonoBehaviour {

    public Notification[] notifications;
    public float notificationTime;
    public float notificationTimer;


	// Use this for initialization
	void Start () {

        foreach (Notification n in notifications)
        {
            if (n.physicalUIObject != null)
            { 
            n.startScale = n.physicalUIObject.transform.localScale;
            n.startPosition = n.physicalUIObject.transform.position;
            // n.onOff.On = false;
            n.onOff.timer = 0;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
        foreach (Notification n in notifications)
        {
            n.Update();
        }
        HideNotificationAfterTimeIsUp();
	}

    public void HideNotificationAfterTimeIsUp()
    {
        if (notificationTimer > 0)
            notificationTimer += Time.deltaTime;

        if ( notificationTimer > notificationTime)
        {
            notificationTime = 0f;
            foreach (Notification n in notifications)
            {
                n.onOff.On = false;
            }
        }
    }

    public void EnableNotification(string name, float time)
    {
        EnableNotification(name);
        notificationTimer = 0.01f;
        notificationTime = time;
    }

    public void EnableNotification(string name)
    {
        Notification n = notifications.FirstOrDefault(p => p.name == name);
        n.onOff.On = true;
        n.physicalUIObject.SetActive(true);
        notificationTimer = 0f;

    }

    public void DisableNotification(string name)
    {
        notifications.FirstOrDefault(p => p.name == name).onOff.On = false;
    }

    [System.Serializable]
    public class Notification
    {
        public enum Transition {Scale,TranslationLeft, TranslationRight};

        public Transition transitionType;
        public string name;
        public OnOffCounter onOff;
        public GameObject physicalUIObject;
        public Vector3 startScale;
        public Vector3 startPosition;
        public Vector3 translationMult;

        public void Update()
        {
            if (physicalUIObject != null)
            {
                onOff.Update();
                if (transitionType == Transition.Scale)
                {
                    physicalUIObject.transform.localScale = startScale * onOff.timerMutated;
                }

                if (transitionType == Transition.TranslationLeft)
                {
                    physicalUIObject.transform.position = startPosition + (translationMult * onOff.timerMutated);
                }

                if (transitionType == Transition.TranslationRight)
                {
                    physicalUIObject.transform.position = startPosition - (translationMult * onOff.timerMutated);
                }
            }
        }
    }

}
