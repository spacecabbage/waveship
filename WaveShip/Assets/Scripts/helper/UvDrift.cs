﻿using UnityEngine;
using System.Collections;

public class UvDrift : MonoBehaviour {

	public Material[] thisMaterial;
    public float speedUp;


	// Use this for initialization
	void Start () {


		//thisMaterial = this.gameObject.renderer.materials[0];
	
	}
	
	// Update is called once per frame
	void Update () {

		int i = 0;


		foreach (Material m in thisMaterial)
		{



			//float speedUp = 1f;

			//if (i == 6)
			//{
			//	speedUp = 1.4f;
			//}

			//if (i == 7) //diamond
			//{
			//	speedUp = 0.7f;
			//}


			m.SetTextureOffset("_MainTex", new Vector2(( 

			                                                       m.GetTextureOffset("_MainTex").x + Time.deltaTime * 0.5f * speedUp ),
			                                                      0 ));

			if (m.GetTextureOffset("_MainTex").x > 1f)
			{
				m.SetTextureOffset("_MainTex", new Vector2(( m.GetTextureOffset("_MainTex").x - 1f ),0 ));
			}

			i++;

		}
	
	}
}
