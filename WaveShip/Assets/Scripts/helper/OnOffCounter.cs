using UnityEngine;
using System.Collections;





[System.SerializableAttribute]

public class OnOffCounter { //: MonoBehaviour {
	

	public bool On;

	public float maxTimer=1f;

	public float timer;
	public float timerMutated;

	public float timerMutatedInversed;
	public AnimationCurve curveEnter ;//= AnimationCurve.Linear(0f,0f,1f,1f);
	public AnimationCurve curveLeave ;//= AnimationCurve.Linear(0f,0f,1f,1f);

	public float timerMutatedMultiplier =1f;

	public float lastSinceStartupTime;

	public float deltaStartup;
	public bool timeScaleZeroProof;


	public bool matured;
	public float speed = 1;
	

//	OnOffCounter()
//	{
//		maxTimer=1;
//	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void Update () {
		
		
		deltaStartup = Time.realtimeSinceStartup - lastSinceStartupTime;
		lastSinceStartupTime = Time.realtimeSinceStartup;
		timerMutated = matured== false ? curveEnter.Evaluate(timer*timerMutatedMultiplier) : curveLeave.Evaluate(timer*timerMutatedMultiplier) ;
		timerMutatedInversed = 1f -timerMutated;
		
		if (On)
		{
			
			matured = false;
			
			if (timeScaleZeroProof)
			{
				timer += deltaStartup * speed;
			}
			else
			{
				timer+= Time.deltaTime* speed;
				
			}
		}
		else
		{
			//dojrzewamy jezeli mamy full
			
			if (timer >= maxTimer-0.17f)
			{
				matured = true;
			}
			
			
	
			if (timeScaleZeroProof)
			{
				timer -= deltaStartup * speed;
			}
			else
			{
				timer-= Time.deltaTime * speed;
				
			}
		}
		
		
		
		if (timer < 0)
		{
			timer = 0;
		}
		
			
		if (timer > maxTimer)
		{
			timer = maxTimer;
		}
	
	}
}
