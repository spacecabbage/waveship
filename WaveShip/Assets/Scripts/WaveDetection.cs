﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveDetection : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    float timeOfLastWave;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.transform.parent.GetComponent<WaveObject>() != null)
        {
            waveTypes typeo = collider.gameObject.transform.parent.GetComponent<WaveObject>().waveType;
            collider.gameObject.transform.parent.GetComponent<WaveObject>().EnableShining();
            GameManager.instance.waveChecker.currentCheckedWave = collider.gameObject.transform.parent.GetComponent<WaveObject>();
            GameManager.instance.waveChecker.currentCheckedWaveType = typeo;
            GameManager.instance.waveChecker.selectedColor = (int)typeo;
            //int typeNumber = (int)typeo;
            //string toString = typeNumber.ToString();
            //Debug.Log("Collided " + toString);
            Debug.Log("Time since last wave: " + (Time.time - timeOfLastWave) + "; BPM: " + 60f / (Time.time - timeOfLastWave));
            timeOfLastWave = Time.time;
        }
    }
}
